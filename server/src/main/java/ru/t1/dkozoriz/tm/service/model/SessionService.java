package ru.t1.dkozoriz.tm.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.dkozoriz.tm.api.service.model.ISessionService;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.repository.model.SessionRepository;

@Service
@NoArgsConstructor
public final class SessionService extends UserOwnedService<Session> implements ISessionService {

    private final static String NAME = "Session";

    @NotNull
    public String getName() {
        return NAME;
    }

    @NotNull
    @Autowired
    @Getter
    private SessionRepository repository;

}