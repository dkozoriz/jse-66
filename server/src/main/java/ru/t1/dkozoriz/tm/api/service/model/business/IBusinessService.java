package ru.t1.dkozoriz.tm.api.service.model.business;

import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.service.model.IUserOwnedService;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.business.BusinessModel;

import java.util.List;

public interface IBusinessService<T extends BusinessModel> extends IUserOwnedService<T> {

    T changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @Nullable List<T> findAll(@Nullable String userId, @Nullable Sort sort);

    T updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

}