package ru.t1.dkozoriz.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.dkozoriz.tm.dto.model.UserOwnedModelDto;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.repository.dto.UserOwnedDtoRepository;

import java.util.List;

public abstract class UserOwnedDtoService<T extends UserOwnedModelDto> extends AbstractDtoService<T>
        implements IUserOwnedDtoService<T> {

    @NotNull
    protected abstract UserOwnedDtoRepository<T> getRepository();

    @Override
    @Nullable
    @Transactional
    public T add(@NotNull final String userId, @Nullable final T model) {
        if (model == null) return null;
        getRepository().saveAndFlush(model);
        return model;
    }

    @Override
    @Transactional
    public void update(@NotNull final String userId, @NotNull final T model) {
        getRepository().saveAndFlush(model);
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        getRepository().deleteByUserId(userId);
    }

    @Override
    @NotNull
    public List<T> findAll(@NotNull final String userId) {
        return getRepository().findAllByUserId(userId);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @Nullable final T model) {
        getRepository().deleteByIdAndUserId(model.getId(), userId);
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final T model = findById(userId, id);
        if (model == null) return;
        remove(userId, model);
    }

    @Override
    @Nullable
    public T findById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return getRepository().findByIdAndUserId(id, userId);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return getRepository().countByUserId(userId);
    }

}