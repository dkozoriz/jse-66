package ru.t1.dkozoriz.tm.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.repository.TaskRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository repository;

    public List<Task> findAll() {
        return repository.findAll();
    }

    @Transactional
    public Task save(Task task) {
        return repository.save(task);
    }

    @Transactional
    public Task update(Task task) {
        if (!repository.existsById(task.getId())) return null;
        return repository.save(task);
    }

    public Task findById(String task) {
        return repository.findById(task).orElse(null);
    }

    public long count() {
        return repository.count();
    }

    @Transactional
    public void deleteById(String task) {
        repository.deleteById(task);
    }

    @Transactional
    public void deleteAll() {
        repository.deleteAll();
    }

}