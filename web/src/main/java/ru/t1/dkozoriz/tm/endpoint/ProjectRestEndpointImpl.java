package ru.t1.dkozoriz.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.api.ProjectRestEndpoint;
import ru.t1.dkozoriz.tm.model.Project;
import ru.t1.dkozoriz.tm.service.ProjectService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/project")
public class ProjectRestEndpointImpl implements ProjectRestEndpoint {

    private final ProjectService projectService;

    @Override
    @GetMapping("/getAll")
    public List<Project> getAll() {
        return projectService.findAll();
    }

    @Override
    @GetMapping("/count")
    public Long count() {
        return projectService.count();
    }

    @Override
    @GetMapping("/get/{id}")
    public Project get(
            @PathVariable("id") String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @PostMapping("/post")
    public Project post(@RequestBody Project project) {
        return projectService.save(project);
    }

    @Override
    @PutMapping("/put")
    public Project put(
            @RequestBody Project project
    ) {
        return projectService.update(project);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(
            @PathVariable("id") String id
    ) {
        projectService.deleteById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        projectService.deleteAll();
    }

}
