package ru.t1.dkozoriz.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.model.Project;

import java.util.List;

public interface ProjectRestEndpoint {

    @GetMapping("/getAll")
    List<Project> getAll();

    @GetMapping("/count")
    Long count();

    @GetMapping("/get/{id}")
    Project get(
            @PathVariable("id") String id
    );

    @PostMapping("/post")
    Project post(@RequestBody Project project);

    @PutMapping("/put")
    Project put(
            @RequestBody Project project
    );

    @DeleteMapping("/delete/{id}")
    void delete(
            @PathVariable("id") String id
    );

    @DeleteMapping("/deleteAll")
    void deleteAll();

}