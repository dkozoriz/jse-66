package ru.t1.dkozoriz.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.dkozoriz.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_web_project")
public class Project implements Serializable {

    private static final long serialVersionUID = 1;

    @Id
    private String id = UUID.randomUUID().toString();

    @Column
    private String name = "";

    @Column
    private String description = "";

    @Column
    private Status status = Status.NOT_STARTED;

    @Column
    private Date created = new Date();

    public Project(final String name) {
        this.name = name;
    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

}