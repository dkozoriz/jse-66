package ru.t1.dkozoriz.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.t1.dkozoriz.tm.api.TaskRestEndpoint;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.service.TaskService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/task")
public class TaskRestEndpointImpl implements TaskRestEndpoint {

    private final TaskService taskService;

    @Override
    @GetMapping("/getAll")
    public List<Task> getAll() {
        return taskService.findAll();
    }

    @Override
    @GetMapping("/count")
    public Long count() {
        return taskService.count();
    }

    @Override
    @GetMapping("/get/{id}")
    public Task get(
            @PathVariable("id") String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @PostMapping("/post")
    public Task post(@RequestBody Task task) {
        return taskService.save(task);
    }

    @Override
    @PutMapping("/put")
    public Task put(
            @RequestBody Task task
    ) {
        return taskService.update(task);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(
            @PathVariable("id") String id
    ) {
        taskService.deleteById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        taskService.deleteAll();
    }

}
