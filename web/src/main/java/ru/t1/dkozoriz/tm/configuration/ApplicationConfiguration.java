package ru.t1.dkozoriz.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.t1.dkozoriz.tm")
public class ApplicationConfiguration {

}