package ru.t1.dkozoriz.t;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.marker.IntegrationCategory;
import ru.t1.dkozoriz.tm.client.ProjectRestEndpointClient;
import ru.t1.dkozoriz.tm.client.TaskRestEndpointClient;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.Task;


@Category(IntegrationCategory.class)
public class TaskTest {

    private static final TaskRestEndpointClient client = TaskRestEndpointClient.getInstance();

    private final Task task1 = new Task("Task 1", "Description 1");
    private final Task task2 = new Task("Task 2", "Description 2");

    @Before
    public void before() {
        client.post(task1);
    }

    @After
    public void after() {
        client.delete(task1.getId());
    }


    @Test
    public void getByIdTest() {
        Assert.assertNotNull(client.get(task1.getId()));
    }

    @Test
    public void addTest() {
        final Task task = new Task("Test Task", "Test");
        client.post(task);
        Assert.assertNotNull(client.get(task.getId()));
        client.delete(task.getId());
    }

    @Test
    public void deleteByIdTest() {
        client.post(task2);
        long expected = client.count() - 1;
        client.delete(task2.getId());
        Assert.assertEquals(expected, (long)client.count());
    }

    @Test
    public void editTest() {
        Assert.assertNotNull(client.get(task1.getId()));
        task1.setName("New name");
        task1.setStatus(Status.IN_PROGRESS);
        task1.setDescription("New Description");
        client.put(task1);
        final Task updatedProject = client.get(task1.getId());
        Assert.assertEquals(task1.getId(), updatedProject.getId());
        Assert.assertEquals(task1.getStatus(), updatedProject.getStatus());
        Assert.assertEquals(task1.getDescription(), updatedProject.getDescription());
    }

    @Test
    public void getAllTest() {
        Assert.assertEquals(client.getAll().size(), (long)client.count());
    }

    @Test
    public void countTest() {
        long count = client.count() + 1;
        final Task task = new Task("Test Task", "Test");
        client.post(task);
        Assert.assertEquals(count, (long)client.count());
    }

}