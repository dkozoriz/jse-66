package ru.t1.dkozoriz.tm.exception;

import org.jetbrains.annotations.NotNull;

public final class EndpointException extends AbstractException {

    public EndpointException(@NotNull final String message) {
        super(message);
    }

}