package ru.t1.dkozoriz.tm.api.listener;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;

public interface IListener {

    void handler(@NotNull ConsoleEvent consoleEvent);

}
