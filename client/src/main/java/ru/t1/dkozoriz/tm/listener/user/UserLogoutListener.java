package ru.t1.dkozoriz.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    public UserLogoutListener() {
        super("logout", "logout current user.");
    }

    @Override
    @EventListener(condition = "@userLogoutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[USER LOGOUT]");
        authEndpoint.logout(new UserLogoutRequest(getToken()));
    }

}